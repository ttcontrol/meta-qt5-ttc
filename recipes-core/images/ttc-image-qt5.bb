DESCRIPTION = "EXPERIMENTAL image based on ttc-image-clutter and including Qt5"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit populate_sdk populate_sdk_qt5

require recipes-core/images/ttc-image-clutter.bb

IMAGE_INSTALL += " \
	qtbase qtbase-fonts qtbase-plugins \
	"

